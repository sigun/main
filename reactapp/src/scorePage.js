import React from "react";
import { meteurl } from "./index.js";

export class ScorePage extends React.Component {
  constructor() {
    super();
    this.state = {
      resp: "wait"
    };
  }

  componentWillMount = () => {
    let url = meteurl + "/scores";
    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        authorization: JSON.parse(localStorage.getItem("TOKEN"))
      }
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        this.setState({
          resp: response
        });
      });
  };

  render() {
    const postItems =
      this.state.resp !== "wait" && this.state.resp !== null
        ? this.state.resp.map(resp => {
            return (
              <div key={resp.id}>
                <h5>
                  {resp.Username}: {resp.Score}
                </h5>
              </div>
            );
          })
        : null;

    return (
      <div>
        <h2>
          {this.state.resp === "wait" ? null : this.state.resp ? (
            <div>
              <div>Scores: {postItems}</div>
            </div>
          ) : (
            <div>No response from server</div>
          )}
        </h2>
      </div>
    );
  }
}
