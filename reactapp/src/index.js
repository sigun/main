import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { LoginPage } from "./loginPage";
import { Game } from "./game";
import { RegisterPage } from "./regPage";
import { HomePage } from "./homePage";
import { AboutPage } from "./aboutPage";
import { LogoutPage } from "./logoutPage";
import { ScorePage } from "./scorePage";
import { StatPage } from "./statPage";
import { PrivateRoute } from "./privateRoute";

export let meteurl = "http://192.168.1.108:5000";

class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <hr />
          <ul>
            <button className="nbutton">
              <Link to="/" className="link">
                Home
              </Link>
            </button>
            {localStorage.getItem("TOKEN") ? (
              <button className="nbutton">
                <Link to="/highscores" className="link">
                  Highscores
                </Link>
              </button>
            ) : null}
            <button className="nbutton">
              <Link to="/about" className="link">
                About
              </Link>
            </button>
            <button className="nbutton">
              <Link to="/game" className="link">
                Play
              </Link>
            </button>
            {!localStorage.getItem("TOKEN") ? (
              <button className="nbutton">
                <Link to="/login" className="link">
                  Login
                </Link>
              </button>
            ) : null}
            {!localStorage.getItem("TOKEN") ? (
              <button className="nbutton">
                <Link to="/signup" className="link">
                  Sign up
                </Link>
              </button>
            ) : null}
            {localStorage.getItem("TOKEN") ? (
              <button className="nbutton">
                <Link to="/stats" className="link">
                  Stats
                </Link>
              </button>
            ) : null}
            {localStorage.getItem("TOKEN") ? (
              <button className="nbutton">
                <Link
                  to="/logout"
                  className="link"
                  onClick={() => window.location.reload()}
                >
                  Logout
                </Link>
              </button>
            ) : null}
          </ul>
          <hr />
          <Route exact path="/" component={HomePage} />
          <Route path="/about" component={AboutPage} />
          <PrivateRoute path="/game" component={Game} />
          <Route path="/login" component={LoginPage} />
          <Route path="/signup" component={RegisterPage} />
          <Route path="/logout" component={LogoutPage} />
          <PrivateRoute path="/highscores" component={ScorePage} />
          <PrivateRoute path="/stats" component={StatPage} />
        </div>
      </Router>
    );
  }
}

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById("root")
);
