import React from "react";
import resim from "./images/resim.PNG";
import resim2 from "./images/resim2.PNG";
import resim3 from "./images/resim3.PNG";

export class HomePage extends React.Component {
  render() {
    return (
      <div>
        <h1>Nasıl Oynanır?</h1>
        <img className="Home" alt="icon" src={resim} />
        <p>
          Kimin başlayacağına karar verin. Bunun için yazı-tura atabilir,
          taş-kağıt-makas oynayabilir ya da yaşça küçük olanın başlamasına izin
          verebilirsiniz. Kimin ‘X’ kimin ‘O’ olduğuna da karar vermelisiniz.
        </p>
        <img className="Home" alt="icon" src={resim2} />
        <p>
          İlk siz oynayacaksanız, köşe ya da merkez karelerinden birini seçin.
          Bu kareler diğerlerinden daha fazla seçenek sunar. İkinci sırada
          oynuyorsanız ve rakibiniz köşelerdeki karelerin biriyle başladıysa,
          tam ortadaki kareyi kullanarak rakibinizin adımlarını
          engelleyebilirsiniz.
        </p>
        <img className="Home" alt="icon" src={resim3} />
        <p>
          alt alta veya üst üste, 'O' ları veya 'X' leri dört defa
          yazabilirseniz hanenize 1 puan yazılır.
        </p>
        <p> İYİ EĞLENCELER... </p>
      </div>
    );
  }
}
