import React from "react";
import { meteurl } from "./index.js";
import { deepClone } from "./util";

function Square(props) {
  return (
    <div
      className={
        props.value === "X"
          ? "square"
          : props.value === "O"
            ? "square2"
            : "square3"
      }
      onClick={props.onClick}
    >
      {props.value}
    </div>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
        id={i}
      />
    );
  }

  render() {
    let status = "Next player: " + (this.props.turn ? "X" : "O");
    return (
      <div>
        <div className="status">{status}</div>
        <div>
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div>
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
          {this.renderSquare(9)}
          {this.renderSquare(10)}
          {this.renderSquare(11)}
        </div>
        <div>
          {this.renderSquare(12)}
          {this.renderSquare(13)}
          {this.renderSquare(14)}
          {this.renderSquare(15)}
          {this.renderSquare(16)}
          {this.renderSquare(17)}
        </div>
        <div>
          {this.renderSquare(18)}
          {this.renderSquare(19)}
          {this.renderSquare(20)}
          {this.renderSquare(21)}
          {this.renderSquare(22)}
          {this.renderSquare(23)}
        </div>
        <div>
          {this.renderSquare(24)}
          {this.renderSquare(25)}
          {this.renderSquare(26)}
          {this.renderSquare(27)}
          {this.renderSquare(28)}
          {this.renderSquare(29)}
        </div>
        <div>
          {this.renderSquare(30)}
          {this.renderSquare(31)}
          {this.renderSquare(32)}
          {this.renderSquare(33)}
          {this.renderSquare(34)}
          {this.renderSquare(35)}
        </div>
      </div>
    );
  }
}

export class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [],
      squares: Array(36).fill(null),
      turn: true,
      bluey: 0,
      greeny: 0
    };
  }

  undoMove() {
    if (!this.state.history.length) {
      return;
    }
    let { history } = this.state;
    const squares = history.pop();

    this.setState(prevState => {
      return {
        history,
        squares,
        turn: !prevState.turn,
        bluey: this.calculatescores(squares)[0],
        greeny: this.calculatescores(squares)[1]
      };
    });
  }

  handleClick(i) {
    let { history, squares, turn } = this.state;
    if (squares[i] != null) {
      return;
    }

    let clonedSquares = deepClone(squares);
    history.push(clonedSquares);
    squares[i] = turn ? "X" : "O";

    this.setState(prevState => {
      return {
        history,
        squares,
        turn: !prevState.turn,
        bluey: this.calculatescores(squares)[0],
        greeny: this.calculatescores(squares)[1]
      };
    });
  }

  calculatescores(squares) {
    let lines = [];
    for (let x = 0; x < 6; x++) {
      for (let y = 0; y < 3; y++) {
        lines.push([6 * x + y, 6 * x + y + 1, 6 * x + y + 2, 6 * x + y + 3]);
      }
    }
    for (let x = 0; x < 6; x++) {
      for (let y = 0; y < 3; y++) {
        lines.push([
          x + 6 * y,
          x + 6 * (y + 1),
          x + 6 * (y + 2),
          x + 6 * (y + 3)
        ]);
      }
    }

    for (let x = 0; x < 3; x++) {
      for (let y = 0; y < 3; y++) {
        lines.push([x + 6 * y, x + 6 * y + 7, x + 6 * y + 14, x + 6 * y + 21]);
      }
    }

    for (let x = 3; x < 6; x++) {
      for (let y = 0; y < 3; y++) {
        lines.push([x + 6 * y, x + 6 * y + 5, x + 6 * y + 10, x + 6 * y + 15]);
      }
    }
    let blueScore = 0;
    let greenScore = 0;
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c, d] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c] &&
        squares[a] === squares[d]
      ) {
        if (squares[a] === "O") {
          greenScore++;
        } else {
          blueScore++;
        }
      }
    }
    return [blueScore, greenScore];
  }

  componentDidUpdate() {
    if (this.state.squares.includes(null)) {
      return null;
    } else if (this.state.greeny > this.state.bluey) {
      return this.greenWins();
    } else if (this.state.bluey > this.state.greeny) {
      return this.blueWins();
    } else {
      return this.tie();
    }
  }

  postInfo = () => {
    let url = meteurl + "/sendscore";

    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        authorization: JSON.parse(localStorage.getItem("TOKEN"))
      },

      body: JSON.stringify({ username: username, message: "Game Over" })
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
      });
  };

  greenWins() {
    this.postInfo();
    alert("Green Wins");
  }

  blueWins() {
    this.postInfo();
    alert("Blue Wins");
  }

  tie() {
    this.postInfo();
    alert("its a TIE!");
  }

  render() {
    return (
      <div className="game">
        <Board
          turn={this.state.turn}
          squares={this.state.squares}
          onClick={i => this.handleClick(i)}
        />
        <div className="game-info">
          <div className="menu" align="center">
            MENU
          </div>

          <div className="bluebox" align="center">
            {this.state.bluey}
          </div>

          <div className="greenbox" align="center">
            {this.state.greeny}
          </div>

          <div
            className="undobutton"
            align="center"
            onClick={() => this.undoMove()}
          >
            UNDO
          </div>
        </div>
      </div>
    );
  }
}
