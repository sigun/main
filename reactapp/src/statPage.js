import React from "react";
import { meteurl } from "./index.js";

export class StatPage extends React.Component {
  constructor() {
    super();
    this.state = {
      resp: "wait"
    };
  }

  componentWillMount = () => {
    let url = meteurl + "/userscores";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        authorization: JSON.parse(localStorage.getItem("TOKEN"))
      },
      body: JSON.stringify({
        Username: JSON.parse(localStorage.getItem("USERNAME")),
        message: "GetStats"
      })
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        this.setState({
          resp: response
        });
      });
  };

  render() {
    console.log(this.state.resp);
    return (
      <div>
        <h2>
          {this.state.resp === "wait" ? null : this.state.resp ? (
            <div>
              <div>Username: {this.state.resp.Username}</div>
              <div>Score: {this.state.resp.Score}</div>
            </div>
          ) : (
            <div>No response from server</div>
          )}
        </h2>
      </div>
    );
  }
}
