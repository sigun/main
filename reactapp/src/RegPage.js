import React from "react";
import "./regPage.css";
import { meteurl } from "./index.js";

export class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { Username: "", email: "", Password: "" };
    //this.handleChange = this.handleChange.bind(this);
    //this.handleSubmit = this.handleSubmit.bind(this);
  }
  postInfo = () => {
    let url = meteurl + "/register";
    console.log(url);
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      },
      body: JSON.stringify(this.state)
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
      });
  };
  handleChangeName(event) {
    let { Username } = this.state;
    Username = event.target.value;
    this.setState({ Username });
  }

  handleChangeMail(event) {
    let { email } = this.state;
    email = event.target.value;
    this.setState({ email });
  }

  handleChangePW(event) {
    let { Password } = this.state;
    Password = event.target.value;
    this.setState({ Password });
  }

  render() {
    if (localStorage.getItem("TOKEN")) {
      return <div>You have to logout first to signup.</div>;
    }
    return (
      <form>
        <div className="inputsReg">
          <label>
            Username:
            <input
              type="text"
              value={this.state.Username}
              className="buttonReg"
              onChange={e => {
                this.setState({ Username: e.target.value });
              }}
            />
            Email:
            <input
              type="text"
              value={this.state.email}
              className="buttonReg"
              onChange={e => {
                this.setState({ email: e.target.value });
              }}
            />
            Password:
            <input
              type="text"
              value={this.state.Password}
              className="buttonReg"
              onChange={e => {
                this.setState({ Password: e.target.value });
              }}
            />
          </label>
        </div>
        <input
          type="submit"
          className="SubmitReg"
          onClick={e => {
            this.postInfo();
            e.preventDefault();
          }}
        />
      </form>
    );
  }
}

export default RegisterPage;
