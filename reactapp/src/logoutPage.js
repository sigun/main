import React from "react";
import { Redirect } from "react-router-dom";

export class LogoutPage extends React.Component {
  componentWillMount() {
    localStorage.clear();
    window.location.reload();
  }
  render() {
    return <Redirect to="/" />;
  }
}
