import React, { Component } from "react";
import { meteurl } from "./index.js";
import { deepClone } from "./util";
import { Link } from "react-router-dom";

export class LoginPage extends Component {
  state = {
    username: "",
    password: "",
    IDhistory: [],
    PWhistory: []
  };

  hishandlerID(x) {
    let { IDhistory, username } = this.state;
    let clonedName = deepClone(username);
    IDhistory.push(clonedName);
    username = x;
    this.setState({
      username,
      IDhistory
    });
  }

  hishandlerPW(x) {
    let { PWhistory } = this.state;
    let { password } = this.state;
    PWhistory.push(password);
    password = x;
    this.setState({
      password,
      PWhistory
    });
  }

  hisundoID = () => {
    if (!this.state.IDhistory.length) {
      return;
    }
    let { IDhistory, username } = this.state;
    username = IDhistory.pop();
    this.setState({
      username,
      IDhistory
    });
  };
  verifyServer = () => {
    let url = meteurl + "/login";
    let data = {
      Username: this.state.username,
      Password: this.state.password
    };
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      },
      body: JSON.stringify(data)
    })
      .then(response => response.json())
      .then(response => {
        if (response.token) {
          localStorage.setItem("TOKEN", JSON.stringify(response.token));
          localStorage.setItem("USERNAME", JSON.stringify(this.state.username));
          console.log(response);
          window.location.reload();
        }
      });
  };

  hisundoPW = () => {
    if (!this.state.PWhistory.length) {
      return;
    }
    let { password, PWhistory } = this.state;
    password = PWhistory.pop();
    this.setState({
      password,
      PWhistory
    });
  };

  render() {
    if (localStorage.getItem("TOKEN")) {
      return <div>You have already logged in.</div>;
    }
    return (
      <div>
        <div>
          <div>
            <h2>If you login you can play the game!</h2>
            <h3>Please Enter ID and password</h3>

            <div className="forrm">
              ID:
              <input
                value={this.state.username}
                className="formID"
                onChange={e => {
                  this.hishandlerID(e.target.value);
                }}
              />
            </div>
            <button
              disabled={this.state.IDhistory.length === 0}
              className="littleundo"
              onClick={this.hisundoID}
            >
              Undo
            </button>
          </div>
          <div>
            <div className="forrm">
              PW:
              <input
                value={this.state.password}
                className="formPW"
                onChange={e => {
                  this.hishandlerPW(e.target.value);
                }}
              />
            </div>
            <button
              disabled={this.state.PWhistory.length === 0}
              className="littleundo"
              onClick={this.hisundoPW}
            >
              Undo
            </button>
          </div>
          <div>
            <button className="littleundo" onClick={this.verifyServer}>
              ENTER
            </button>
          </div>
        </div>
        <div>
          <h2>If you dont have an account you can sign up</h2>
          <Link to="/signup" className="button">
            Sign up!
          </Link>
        </div>
      </div>
    );
  }
}
