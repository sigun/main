import React from "react";

export const AboutPage = () => (
  <div>
    <h2>Credits:</h2>
    <p>Frontend(react), Tic-tac-toe Game : Ömer Çavdar</p>
    <p>Backend(mongodb, expressjs , server) : Mete Durlu </p>
    <p> Register Page : Esra Çini </p>
  </div>
);
