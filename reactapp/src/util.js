export let deepClone = obj => {
  return JSON.parse(JSON.stringify(obj));
};
