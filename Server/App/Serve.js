const express = require("express");
const router = express.Router();
const Student = require("../models/Student");
const jwt = require("jsonwebtoken");
var AutV = require("../Functions/appf");
var nchk = require("../Functions/namechk")
const cors = require("cors")
const Score = require("../models/Scores")


router.use(cors());

router.post("/register", nchk, function (req, res, next) {//Register new user
    var student = new Student(req.body);
    res.send(student);
    student.save();
    var score = new Score({ "Username": req.body.Username })
    score.save();
    jwt.sign(student, "secretkey", { expiresIn: '3h' }, function (err, token) {
        console.log({ token });
        res.json({ success: true, token: token, message: "Register Successful" });
    })
});

router.post("/login", function (req, res, next) {//token + login check
    Student.findOne({ Username: req.body.Username }, function (err, user) {
        if (err) throw err;
        if (!user) {
            res.json({ success: false, message: "user not found" });
        }
        else if (user) {
            if (user.Password !== req.body.Password) {
                res.json({ success: false, message: "false password" });
            }
            else {
                const payload = {
                    Username: user.Username,
                    Password: user.Password
                };
                jwt.sign(payload, "secretkey", { expiresIn: '3h' }, function (err, token) {
                    res.json({ success: true, token: token, message: "Login Successful" });
                });
            }
        }
    })
});

router.get("/users", AutV, function (req, res, next) {//User List
    Student.find(function (err, docs) {
        if (err) return next(err);
        res.send(docs);
    });
});

router.get("/users/:id", AutV, function (req, res, next) {//Single user by id
    Student.findOne({ _id: req.params.id }, function (err, docs) {
        if (err) return next(err);
        res.json(docs);
    });
});

router.get("/play", AutV, function (req, res, next) {
    res.json({ message: "WELCOME TO TICK-TACK-TOE v 5.0" });
});

router.post("/users", AutV, function (req, res, next) {// User save by hand
    var student = new Student(req.body);
    res.send(student);
    student.save();
});

router.put("/users/:id", AutV, function (req, res, next) {//ADMIN CONTROLS
    Student.findOneAndUpdate({ _id: req.params.id }, req.body, function () {
        res.send({ type: "UPDATED" });
    });
});

router.delete("/users/:id", AutV, function (req, res, next) {//ADMIN CONTROLS
    Student.findByIdAndRemove({ _id: req.params.id }, function (err) { res.send({ type: "DELETED" }); });
});

router.post("/test/:id", function (req, res, next) {//TESTING
    Score.findOneAndUpdate({ Username: req.body.Username }, { $inc: { "Win": +1 } }, function (err, docs) {
        if (err) res.json({ message: "User not found" });
        res.json(docs);
    });
});

module.exports = router;