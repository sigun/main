const express = require("express");
const router = express.Router();
const Score = require("../models/Scores");
var AutV = require("../Functions/appf");
const cors = require("cors")
const DecoderT = require("../Functions/tokendecoder")

router.use(cors());

router.get("/scores", AutV, function (req, res, next) {//Highscore Listesi
    Score.find(function (err, docs) {
        if (err) return next(err);
        res.json(docs);
    });
});

router.get("/userscores", AutV, function (req, res, next) { //STATS
    var user = DecoderT(req, res);
    Score.findOne({ Username: user.Username }, function (err, docs) {
        if (!docs)
            res.json({ success: false, message: "Failed to find Score" });
        else
            res.json(docs);
    });
});

router.post("/gamescores", AutV, function (req, res, next) {//Highscore sender
    var user1 = DecoderT(req, res);
    console.log(user1.Username)
    Score.findOne({ Username: user1.Username }, function (err, user) {
        if (user) {
            if (user.Score < req.body.Score1) {
                Score.findOneAndUpdate({ Username: user1.Username }, { $set: { "Score": req.body.Score1 } }, function (err) {
                    if (err) return err;
                })
            }
        }
    })
    Score.findOne({ Username: req.body.Username2 }, function (err, user) {
        if (user) {
            if (user.Score < req.body.Score2) {
                Score.findOneAndUpdate({ Username: req.body.Username2 }, { $set: { "Score": req.body.Score2 } }, function (err) {
                    if (err) return err;
                })
            }
        }
    })
    res.json({ message: "Users Found and Highscores Updated" });
    /*Score.forEach(element => {
        Score.findOneAndUpdate({ Username: element }, {
            "Score": elemnet.score
        });
    });*/
});

router.delete("/sendscore/:id", AutV, function (req, res, next) {
    Score.findByIdAndRemove({ _id: req.params.id }, function (err) { res.json({ success: true, message: "DELETED" }); });
});

module.exports = router;