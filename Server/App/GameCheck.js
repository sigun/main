const express = require("express");
const router = express.Router();
const Student = require("../models/Student");
const Score = require("../models/Scores");
var AutV = require("../Functions/appf");
const cors = require("cors")
const DecoderT = require("../Functions/tokendecoder")

router.use(cors());

router.post("/gamecheck", AutV, function (req, res, next) {//Username check at the beginning of the game
    var us = DecoderT(req, res);
    Student.findOne({ Username: us.Username }, function (err, user) {
        Student.findOne({ Username: req.body.Username2 }, function (err, user1) {
            if ((user && user1) && user.Username != user1.Username) {
                res.json({ success: true, message: "Users Found" });
            }
            else {
                res.json({ success: false, message: "One or both users are not Found And Users Cannot Be The Same" });
            }
        })
    })
});

router.post("/sendstats", AutV, function (req, res, next) { //Win Lose Tie
    if (req.body.Score1 > req.body.Score2) {
        Score.findOneAndUpdate({ Username: req.body.Username1 }, { $inc: { "Win": 1 } }, function () { })
        Score.findOneAndUpdate({ Username: req.body.Username2 }, { $inc: { "Lose": 1 } }, function () {
            res.json({ success: true, message: "Win Lose Updated" })
        })
    }
    else if (req.body.Score1 < req.body.Score2) {
        Score.findOneAndUpdate({ Username: req.body.Username2 }, { $inc: { "Win": 1 } }, function () { })
        Score.findOneAndUpdate({ Username: req.body.Username1 }, { $inc: { "Lose": 1 } }, function () {
            res.json({ success: true, message: "Win Lose Updated" })
        })
    }
    else {
        Score.findOneAndUpdate({ Username: req.body.Username2 }, { $inc: { "Tie": 1 } }, function () { })
        Score.findOneAndUpdate({ Username: req.body.Username1 }, { $inc: { "Tie": 1 } }, function () {
            res.json({ success: true, message: "Ties Updated" })
        })
    }
});


module.exports = router;