const express = require("express")
const users = require("./App/Serve")
const scores = require("./App/Scores")
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const GameCheck = require("./App/GameCheck")

const app = express();
mongoose.connect("mongodb://127.0.0.1:27017/list", { useNewUrlParser: true });
mongoose.Promise = global.Promise;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(users);
app.use(scores);
app.use(GameCheck);

app.listen(process.env.port || 5000, function () {
    console.log("Server UP! at port : " + (process.env.port || 5000))
});
