const mongoose = require('mongoose')

const studentSchema = new mongoose.Schema({
    Username: {
        type: String,
        required: [true, 'NAME REQUIRED!']
    },
    Password: {
        type: String,
        required: [true, 'PASS REQUIRED!']
    },
    active: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model("Student", studentSchema);