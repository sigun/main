const mongoose = require('mongoose')

const scoreSchema = new mongoose.Schema({
    Username: {
        type: String,
        required: [true, 'NAME REQUIRED!']
    },
    Score: {
        type: Number,
        default: 0
    },
    Win: {
        type: Number,
        default: 0
    },
    Lose: {
        type: Number,
        default: 0
    },
    Tie: {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model("Score", scoreSchema);