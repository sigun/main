const Student = require("../models/Student");


function namecheck(req, res, next) {
    Student.findOne({ Username: req.body.Username }, function (err, user) {
        if (user) {
            return res.status(403).send({
                success: false,
                message: 'Username Taken.'
            });
        }
        else {
            next();
        }
    })

}
module.exports = namecheck;