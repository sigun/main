const jwt = require("jsonwebtoken");

function DecoderT(req, res, next) {
    var token = req.body.token || req.query.token || req.headers['authorization'];
    var x = jwt.decode(token);
    return x;
}

module.exports = DecoderT;