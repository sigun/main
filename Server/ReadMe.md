
Server.js --> is the main server file it opens the local port and starts the server.
Student.js --> is the student schema for the mongoDB to store data.
Serve.js --> has the routes and the basic GET - POST - PUT - DELETE functions controlled with
			 Jsonwebtoken system.
Appf.js --> contains jsonwebtoken token check function.

1-- Install Node.js version v8.11.3\n
2-- Install latest version of MongoDB
3-- Fire up express embeded server.js file using Node.js
  3.1--Use " cd " in Node.js to navigate.
  3.2--Type npm "install" in server.js directory.
  3.3--Type "nodemon start".
  3.4--Test by typing localhost:3000 in your browser or using Postman(3rd party software)
4-- Create "c:\data\db" directory(MongoDB uses it to store data)
5--"C:\Program Files\MongoDB\Server\4.0\bin\" Find this directory then run mongod.exe.
6--Connect the data stream by changing connection address with using mongoose.
7--Open terminal
8--Now you have the running mongoDB server tethered with an Express framework.
